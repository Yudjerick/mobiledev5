package com.example.pract5;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.pract5.databinding.FragmentOneBinding;
public class FragmentOne extends Fragment {
    private FragmentOneBinding binding;
    private String txt1 = "1";
    private String txt2 = "2";

    public FragmentOne() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentOneBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments()!=null){
            binding.editText1.setText(getArguments().getString("key"));
            Toast.makeText(getContext(), txt2, Toast.LENGTH_SHORT).show();
        }

        binding.openFr2.setOnClickListener(view1 -> {
            Bundle bundle = new Bundle();
            bundle.putString("key", binding.editText1.getText().toString());
            Navigation.findNavController(view).navigate(R.id.action_fragmentOne_to_fragmentTwo, bundle);
        });
        binding.openFr3.setOnClickListener(view1 -> {
            Bundle bundle = new Bundle();
            bundle.putString("key", binding.editText1.getText().toString());
            txt1 = binding.editText1.getText().toString();
            Navigation.findNavController(view).navigate(R.id.action_fragmentOne_to_fragmentThree, bundle);
        });
    }
}