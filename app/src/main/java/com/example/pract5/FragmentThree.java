package com.example.pract5;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.pract5.databinding.FragmentThreeBinding;

public class FragmentThree extends Fragment {
    private FragmentThreeBinding binding;

    public FragmentThree() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentThreeBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.fr3EditText.setText(getArguments().getString("key"));

        binding.backBtn2.setOnClickListener(view1->{
            Bundle bundle = new Bundle();
            bundle.putString("key", binding.fr3EditText.getText().toString());
            Navigation.findNavController(view).navigate(R.id.action_fragmentThree_to_fragmentOne, bundle);
        });
    }
}