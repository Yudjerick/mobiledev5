package com.example.pract5;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pract5.databinding.FragmentTwoBinding;

public class FragmentTwo extends Fragment {
    FragmentTwoBinding binding;

    public FragmentTwo() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentTwoBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.fr2EditText.setText(getArguments().getString("key"));

        binding.backBtn.setOnClickListener(view1->{
            Bundle bundle = new Bundle();
            bundle.putString("key", binding.fr2EditText.getText().toString());
            Navigation.findNavController(view).navigate(R.id.action_fragmentTwo_to_fragmentOne, bundle);
        });
    }
}